const exp = require('express');
const bodyParser = require('body-parser');
const FormData = require('form-data');
const axios = require('axios');
const uuid = require('uuid');
var moment = require('moment');
const pdf2base64 = require('pdf-to-base64');
const winston = require('winston');
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');

var transport = new winston.transports.DailyRotateFile({
    filename: 'RecruitingInterface-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '200m',
    maxFiles: '14d'
});

const logger = winston.createLogger({
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}` + (info.splat !== undefined ? `${info.splat}` : " "))
    ),
    transports: [
        transport
    ]
});

var de = require('./sidestep_modules/EloIxClient');
var ixConf = {
    userName: "Administrator",
    password: "elo2pass@@"
};

var connFact;
var ixConn;

const portalConfig = {
    protocol: 'https',
    request: {
        auth: 'elo_api:EloApi123',
        host: 'fuchs-soehne.info',
        //port: '8080',
        path: '',
        method: 'get'
    },
    encoding: 'utf8'
};

var app = exp();
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.get('', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    res.sendStatus(200);
});

app.post('/PutJobAdvertisement', function (req, res) {
    try {
        res.set({ 'Access-Control-Allow-Origin': "*" });
        var id = req.body.uuid;
        if (req.body.type == 'new') {
            id = uuid.v4();
        };

        var formData = new FormData();
        formData.append("id", id);
        prepareFormData(req.body.keys, formData);

        axios.put(`https://fuchs-soehne.info/resources/karriere/api/stellenanzeige/${id}`, formData,
            { headers: formData.getHeaders(), auth: { username: "elo_api", password: "EloApi123" } }).then(response => {


                var url = "";
                try {
                    url = response.data.url;
                } catch (e) {

                }
                res.send({ id: id, url: url })
            }).catch(err => {
                console.log("Error: " + err); res.sendStatus(500)
            });
    } catch (e) {
        res.sendStatus(500);
    }
});

app.post('/GetJobAdveriseMent', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    axios.get(`https://fuchs-soehne.info/app/karriere/stellenanzeigen/${req.body.id}`, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
        res.send({ url: response.data.url })
    }).catch(err => { console.log("err") });
});

app.post('/DeleteJobAdvertisement', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });

    axios.delete(`https://fuchs-soehne.info/resources/karriere/api/stellenanzeige/${req.body.id}`, { auth: { username: "elo_api", password: "EloApi123" } })
        .then(response => {

            res.send({ "result": true });
        }).catch(err => {
            console.log("Error: " + err);
        });
});

app.post('/IsOnline', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    axios.get(req.body.url, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {

        res.sendStatus(200);
    }).catch(err => {
        res.send(err);
    });
});

app.get('/GetHeaderImages', function (req, res) {

    axios.get("https://fuchs-soehne.info/resources/karriere/api/bilder", { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
        var d = { images: response.data };
        res.set({ 'Access-Control-Allow-Origin': "*" })
        res.send(d)
    }).catch(err => { res.sendStatus(500) });
});

function prepareFormData(keyValues, formData) {
    keyValues.forEach((kv) => {
        if (kv.name == "headerbild_id") {
            var obj = kv.value;
            formData.append(kv.name, JSON.stringify(obj));
        } else {
            formData.append(kv.name, kv.value);
        }
    });
}

app.get('/GetApplications', function (req, res) {
    axios.get("https://fuchs-soehne.info/resources/karriere/api/bewerbungen", { auth: { username: "elo_api", password: "EloApi123" } }).then(response => { }).catch(err => { res.sendStatus(500) });
});

function getAllApplications() {
    return new Promise((resolve, reject) => {
        axios.get("https://fuchs-soehne.info/resources/karriere/api/bewerbungen", { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
            resolve(response.data);
        }).catch(err => {
            reject(err);
        });
    });
}

async function AdjustApplications() {
    logger.info("Start");
    try {
        connFact = new de.elo.ix.client.IXConnFactory('http://rb-el-01:9090/ix-eloFUCHS/ix', "RecruitingInterface", "1.0");
        ixConn = await connFact.create("Administrator", ixConf.password);
    } catch (e) {
        logger.error(`Error Creating ELO-Connection: ${e}`);
        return;
    }

    try {
        var allApplications = await getAllApplications();
    } catch (e) {
        logger.error(`Error getting Applications: ${e}`);
        return;
    }
    logger.info(`Found Applications: ${allApplications.length}`);

    for (var i = 0; i < allApplications.length; i++) {
        try {
            var details = await getApplicationDetails(allApplications[i].id);

            try {
                var exists = await checkRequisitionExists(details.jobId);
                logger.info(`Requisition exists: ${JSON.stringify(exists)}`);
            } catch (e) {
                console.log("Error checking Requisition exists: " + e);
                continue;
            }

            if (!exists || exists.data.length == 0) {
                //console.log("Delete Application because no Requsition Exist");
                /*axios.delete(`https://fuchs-soehne.info/resources/karriere/api/bewerbungen/${details.id}`,{auth:{username:"elo_api",password:"EloApi123"}}).then(response=>{

                }).catch(err=>{console.log("err");});*/
                continue;
            };

            try {
                logger.info(`Check Candidate Exists`);
                var candidateExists = await checkCandidateExists(details.id);
                logger.info(`Result: ${JSON.stringify(candidateExists)}`);
            } catch (e) {
                continue;
            }

            if (!candidateExists) {
                try {
                    logger.info(`Create candidate`);
                    var candidate = await createCandidate(exists, details);
                    logger.info(`candidate created: ${JSON.stringify(candidate)}`);
                } catch (e) {
                    logger.error(`error creating candindate ${e}`)
                    continue;
                }
                logger.info(`Found: ${details.anhaenge.length} Attachments`);
                for (var j = 0; j < details.anhaenge.length; j++) {
                    try {
                        var config = await getDocumentConfig(details.anhaenge[j]);
                        logger.info(`Create attachment with type ${config.type} and name: ${config.name}`);
                        await createSord(config, candidate.data.objId);
                        logger.info("Attachment created");
                    } catch (e) {
                        logger.error(`Error creating attachment: ${e}`);
                        continue;
                    }
                }
                try {
                    logger.info(`Start Workflow call`);
                    await startWorkFlow(exists, candidate);
                    logger.info(`Workflow started`);
                } catch (e) {
                    logger.error(`Error starting Workflow: ${e}`);
                    continue;
                }
            } else {
                //Anzahl Anhänge überprüfen
                try {
                    logger.info("Check all Attachments uploaded");
                    var allAttachmentsDone = await checkHasAllAttachments(details.anhaenge.length, candidateExists.objid);
                    if (allAttachmentsDone) {
                        logger.info("Delete Application, all Attachments are uploaded");
                        axios.delete(`https://fuchs-soehne.info/resources/karriere/api/bewerbungen/${details.id}`, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {

                        }).catch(err => { console.log("err"); });
                    } else {
                        //TODO Anhänge hochladen
                    }
                } catch (e) {
                    continue;
                }
            }
        } catch (e) {
            console.log("Error getting details for Application: " + allApplications[i].id);
            console.log("Error : " + e);
            continue;
        }
    }
};

async function startWorkFlow(exists, candidate) {
    return new Promise((resolve, reject) => {
        try {
            ixConn.ix().checkoutSord(exists.data[0].objid, ixConn.getCONST().SORD.mbAll, ixConn.getCONST().LOCK.NO, function res(s) {
                var wfstarted = false;
                for (var i = 0; i < s.objKeys.length; i++) {
                    if (s.objKeys[i].name == "RECRUITING_REQUISITION_RECRUITER") {
                        if (s.objKeys[i].data[0] != "") {
                            wfstarted = true;
                            ixConn.ix().checkoutUser(s.objKeys[i].data[0], ixConn.getCONST().CHECKOUT_USERS.BY_IDS, ixConn.getCONST().LOCK.NO, function result(userInfo) {
                                var flowInfo = new de.elo.ix.client.StartWorkflowInfo();
                                flowInfo.flowName = "Bewerbung " + candidate.candidateName;
                                flowInfo.flowOwner = userInfo.id;
                                flowInfo.templFlowId = "sol.recruiting.Candidate.Create";
                                ixConn.ix().startWorkFlow2(candidate.data.objId, flowInfo, function r() {
                                    resolve(true);
                                });
                            });
                        }
                    }
                }
                if (wfstarted == false) {
                    var flowInfo = new de.elo.ix.client.StartWorkflowInfo();
                    flowInfo.flowName = "Bewerbung " + candidate.candidateName;
                    flowInfo.flowOwner = "36";
                    flowInfo.templFlowId = "sol.recruiting.Candidate.Create";
                    ixConn.ix().startWorkFlow2(candidate.data.objId, flowInfo, function r() {
                        resolve(true);
                    });
                }
            });
        } catch (e) {
            reject(e);
        }
    });

}

function getBearer() {
    return new Promise((resolve, reject) => {
        axios.post('https://fuchs-soehne.info/resources/security/accesstoken', { userid: "elo_api", password: "EloApi123" }).then(response => {
            resolve(response.data.token);
        }).catch(err => {
            reject(err);
        });
    });
};

function getDocumentConfig(a) {
    return new Promise((resolve, reject) => {
        getBearer().then((token) => {
            axios.get(a, { responseEncoding: 'binary', responseType: 'arraybuffer', headers: { Authorization: `Bearer ${token}` } }).then(response => {
                var typeName = response.headers['content-disposition'].match(/filename="(.{1,}\.\w{1,})"/)[1].split(".");
                var conf = {
                    type: typeName.pop(),
                    name: typeName.join("."),
                    base64Content: response.data.toString('base64')
                };

                resolve(conf);
            }).catch(err => { console.log(err); reject(err) });
        }).catch(err => {
            reject(err);
        });
    });
}

function createSord(data, objId) {
    return new Promise((resolve, reject) => {
        var config = {
            base64Content: data.base64Content,
            cfg: {
                maskName: "Dokument",
                pictureName: data.name,
                extension: data.type,
                type: data.type,
            },
            extension: data.type,
            objId: objId
        };
        try {
            ixConn.ix().executeRegisteredFunctionString("RF_sol_common_document_service_UploadFile", JSON.stringify(config), function (res) {
                console.log("Uploaded File");
                console.log(res);
                resolve();
            });
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
};

function checkCandidateExists(id) {
    return new Promise((resolve, reject) => {
        try {
            var cfg = {
                asAdmin: true,
                log: "RF_ResultSet",
                connNr: 1,
                query: `select mapid as objid from eloFUCHS.dbo.map_objekte m inner join eloFUCHS.dbo.objekte o on m.mapid = o.objid where mapkey = 'RECRUITING_CANDIDATE_INTERFACE_ID' and mapvalue = '${id}' and o.objstatus = 0`
            };
            var res = JSON.parse(ixConn.ix().executeRegisteredFunctionString("RF_ResultSet", JSON.stringify(cfg)));
            if (res && res.data.length >= 1) {
                resolve(res.data[0]);
            } else {
                resolve(false);
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
}

function createCandidate(exists, applicationDetails) {
    return new Promise(async (resolve, reject) => {
        try {
            var items = ixConn.ix().checkoutMap("objekte", exists.data[0].objid, null, de.elo.ix.client.LockC.NO).mapItems;
            console.log(items.RECRUITING_REQUISITION_GUID.value);
            var anrede = "";
            var gender = "";
            var geburtsdatum = "";
            var id = applicationDetails.id
            switch (applicationDetails.anrede) {
                case "Männlich":
                    gender = "M - Männlich";
                    anrede = "Sehr geehrter Herr";
                    break;
                case "Weiblich":
                    gender = "F - Weiblich";
                    anrede = "Sehr geehrte Frau";
                    break;
                case "Divers":
                    gender = "X - Divers";
                    anrede = "Guten Tag";
                    break;
            }
            if (applicationDetails.geburtsdatum) {
                geburtsdatum = moment(applicationDetails.geburtsdatum).format("YYYYMMDD");
            }
            //Bewerberakte anlegen
            var cfg = {
                "template": {
                    "name": "Default"
                },
                "sordMetadata": {
                    "mapKeys": {
                        "RECRUITING_REQUISITION_GUID": items.RECRUITING_REQUISITION_GUID.value,
                        "RECRUITING_POSTING_GUID": applicationDetails.jobId,
                        "RECRUITING_CANDIDATE_STATE": "",
                        "RECRUITING_CANDIDATE_PRIVATEEMAIL": applicationDetails.email,
                        "RECRUITING_CANDIDATE_PRIVATEMOBILE": applicationDetails.tel,
                        "RECRUITING_CANDIDATE_STREETADDRESS": applicationDetails.anschrift.strasse,
                        "RECRUITING_CANDIDATE_PRIVATEPHONE": applicationDetails.tel,
                        "RECRUITING_CANDIDATE_INTERFACE_ID": id,
                        "RECRUITING_ATTENTION": applicationDetails.aufmerksam,
                        "CANDIDATE_ANREDE": anrede
                    },
                    "objKeys": {
                        "RECRUITING_CANDIDATE_FIRSTNAME": applicationDetails.vorname,
                        "RECRUITING_CANDIDATE_LASTNAME": applicationDetails.name,
                        "RECRUITING_CANDIDATE_CITY": applicationDetails.anschrift.ort,
                        "RECRUITING_CANDIDATE_COUNTRY": applicationDetails.anschrift.land,
                        "SOL_TYPE": "RECRUITING_CANDIDATE",
                        "RECRUITING_CANDIDATE_POSTALCODE": applicationDetails.anschrift.plz,
                        "RECRUITING_CANDIDATE_PRIVATEEMAIL": applicationDetails.email,
                        "RECRUITING_CANDIDATE_BIRTHDAY": geburtsdatum,
                        "RECRUITING_CANDIDATE_GENDER": gender
                    }
                }
            };
            var res = await ixConn.ix().executeRegisteredFunctionString("RF_sol_recruiting_function_CreateCandidateHeadlessStrict", JSON.stringify(cfg));
            res = JSON.parse(res);
            res.candidateName = applicationDetails.vorname + " " + applicationDetails.name;
            res.candidateId = id;
            resolve(res);

        } catch (e) {
            console.log(e);
            reject(false);
        }
    });
}

function getApplicationDetails(id) {
    return new Promise((resolve, reject) => {
        axios.get(`https://fuchs-soehne.info/resources/karriere/api/bewerbungen/${id}`, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
            resolve(response.data);
        }).catch(err => { resolve(null) });
    });
}

function checkRequisitionExists(jobId) {
    return new Promise((resolve, reject) => {
        try {
            var cfg = {
                asAdmin: true,
                log: "RF_ResultSet",
                connNr: 1,
                query: `select mapid as objid from eloFUCHS.dbo.map_objekte m inner join eloFUCHS.dbo.objekte o on m.mapid = o.objid where mapkey ='POSTING_ID' and mapvalue = '${jobId}' and o.objstatus = 0`
            };
            var res = ixConn.ix().executeRegisteredFunctionString("RF_ResultSet", JSON.stringify(cfg));
            res = JSON.parse(res);

            resolve(res);
        }
        catch (e) {
            reject(e);
        }
    });
}

function checkHasAllAttachments(attachmentCount, objid) {
    return new Promise((resolve, reject) => {
        try {
            var cfg = {
                asAdmin: true,
                log: "RF_ResultSet",
                connNr: 1,
                query: `select COUNT(*) as 'count' from eloFUCHS.dbo.objekte where objparent ='${objid}' and objstatus = 0`
            };
            var res = JSON.parse(ixConn.ix().executeRegisteredFunctionString("RF_ResultSet", JSON.stringify(cfg)));

            if (res && res.data) {
                logger.info(`Found ${JSON.stringify(res.data[0].count)} / ${attachmentCount} attachments`);
                if (String(attachmentCount) == String(res.data[0].count)) {
                    resolve(true);
                }
            }
            resolve(false);
        }
        catch (e) {
            logger.error(e);
            reject(e);
        }
    });
}
AdjustApplications();
setInterval(AdjustApplications, 600000);

app.listen(3000);
console.log("App listen on Port 3000");
const exp = require('express');
const FormData = require('form-data');
const axios = require('axios');
const axiosRetry = require('axios-retry');
const setupCache = require('axios-cache-adapter');
const uuid = require('uuid');
var moment = require('moment');
const winston = require('winston');
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');

var transport = new winston.transports.DailyRotateFile({
    dirname: '.\\log',
    filename: 'RecruitingInterface-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: false,
    maxSize: '200m',
    maxFiles: '14d'
});

const logger = winston.createLogger({
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}` + (info.splat !== undefined ? `${info.splat}` : " "))
    ),
    transports: [
        transport
    ]
});

var de = require('./sidestep_modules/EloIxClient');
var ixConf = {
    userName: "Administrator",
    password: "elo2pass@@"
};

var connFact;
var ixConn;

const portalConfig = {
    protocol: 'https',
    request: {
        auth: 'elo_api:EloApi123',
        host: 'fuchs-soehne.info',
        //port: '8080',
        path: '',
        method: 'get'
    },
    encoding: 'utf8'
};

var app = exp();
app.use(exp.urlencoded({ extended: true }));
app.use(exp.json());

app.get('', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    res.sendStatus(200);
});

app.post('/PutJobAdvertisement', function (req, res) {
    try {
        res.set({ 'Access-Control-Allow-Origin': "*" });
        var id = req.body.uuid;
        if (req.body.type == 'new') {
            id = uuid.v4();
        };

        var formData = new FormData();
        formData.append("id", id);
        prepareFormData(req.body.keys, formData);

        axiosRetry(axios, { retries: 3 });
        axios.put(`https://fuchs-soehne.info/resources/karriere/api/stellenanzeige/${id}`, formData,
            { headers: formData.getHeaders(), auth: { username: "elo_api", password: "EloApi123" } }).then(response => {

                var url = "";
                try {
                    url = response.data.url;
                } catch (e) {

                }
                res.send({ id: id, url: url })
            }).catch(err => {
                logger.error("PutJobAdvertisement - Error: " + err);
                res.sendStatus(500)
            });
    } catch (e) {
        res.sendStatus(500);
    }
});

app.post('/GetJobAdveriseMent', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    axiosRetry(axios, { retries: 3 });
    axios.get(`https://fuchs-soehne.info/app/karriere/stellenanzeigen/${req.body.id}`, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
        res.send({ url: response.data.url })
    }).catch(err => { logger.error("GetJobAdveriseMent - Error: " + err); });
});

app.post('/DeleteJobAdvertisement', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    axiosRetry(axios, { retries: 3 });

    axios.delete(`https://fuchs-soehne.info/resources/karriere/api/stellenanzeige/${req.body.id}`, { auth: { username: "elo_api", password: "EloApi123" } })
        .then(response => {

            res.send({ "result": true });
        }).catch(err => {
            logger.error("DeleteJobAdvertisement - Error: " + err);
        });
});

app.post('/IsOnline', function (req, res) {
    res.set({ 'Access-Control-Allow-Origin': "*" });
    axiosRetry(axios, { retries: 3 });

    axios.get(req.body.url, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {

        res.sendStatus(200);
    }).catch(err => {
        logger.error("IsOnline - Error: " + err);
        res.sendStatus(404);
    });
});

app.get('/GetHeaderImages', function (req, res) {
    const cache = setupCache({
        maxAge: 15 * 60 * 1000
    })
    const headerImageApi = axios.create({
        adapter: cache.adapter
    })
    axiosRetry(headerImageApi, { retries: 3 });
    headerImageApi.get("https://fuchs-soehne.info/resources/karriere/api/bilder", { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
        var d = { images: response.data };
        res.set({ 'Access-Control-Allow-Origin': "*" })
        res.send(d)
    }).catch(err => {
        logger.error("GetHeaderImages - Error: " + err);
        res.sendStatus(500)
    });
});

function prepareFormData(keyValues, formData) {
    keyValues.forEach((kv) => {
        if (kv.name == "headerbild_id") {
            var obj = kv.value;
            formData.append(kv.name, JSON.stringify(obj));
        } else {
            formData.append(kv.name, kv.value);
        }
    });
}

app.get('/GetApplications', function (req, res) {
    axiosRetry(axios, { retries: 3 });
    axios.get("https://fuchs-soehne.info/resources/karriere/api/bewerbungen", { auth: { username: "elo_api", password: "EloApi123" } }).then(response => { }).catch(err => { res.sendStatus(500) });
});

function getAllApplications() {
    return new Promise((resolve, reject) => {
        axiosRetry(axios, { retries: 3 });
        axios.get("https://fuchs-soehne.info/resources/karriere/api/bewerbungen", { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
            resolve(response.data);
        }).catch(err => {
            reject(err);
        });
    });
}

async function AdjustApplications() {
    logger.info("------- Start -------");
    try {
        connFact = new de.elo.ix.client.IXConnFactory('http://rb-el-01:9090/ix-eloFUCHS/ix', "RecruitingInterface", "1.0");
        ixConn = await connFact.create(ixConf.userName, ixConf.password);
    } catch (e) {
        logger.error(`Error creating ELO-Connection: ${e}`);
        return;
    }

    try {
        var allApplications = await getAllApplications();
    } catch (e) {
        logger.error(`Error getting applications: ${e}`);
        return;
    }
    logger.info(`Found ${allApplications.length} application to work on.`);

    for (var i = 0; i < allApplications.length; i++) {
        try {
            logger.info(`Getting details for application no ` + (i + 1));
            var details = await getApplicationDetails(allApplications[i].id);

            try {
                logger.info(`Check requisition for this application exists`);
                var exists = await checkRequisitionExists(details.jobId);
                logger.info(`Found: ${JSON.stringify(exists)}`);
            } catch (e) {
                logger.error("Error checking requisition exists: " + e);
                continue;
            }

            if (!exists || exists.data.length == 0) {
                continue;
            };

            try {
                logger.info(`Check first if the candidate already exists`);
                var candidateExists = await checkCandidateExists(details.id);
                logger.info(`Result: ${JSON.stringify(candidateExists)}`);
            } catch (e) {
                continue;
            }

            //Create the candidate
            if (!candidateExists) {
                logger.info(`Need to create the candidate`);
                try {
                    var candidate = await createCandidate(exists, details);
                    logger.info(`Candidate created: ${JSON.stringify(candidate)}`);
                } catch (e) {
                    logger.error(`Error creating candindate ${e}`)
                    continue;
                }
                logger.info(`Found ${details.anhaenge.length} attachments`);

                var expectedAttachments = [new de.elo.ix.client.KeyValue("EXPECTED_ATTACHMENTS", details.anhaenge.length)];
                ixConn.ix().checkinMap(ixConn.getCONST().MAP_DOMAIN.DOMAIN_SORD, candidate.data.objId, candidate.data.objId, expectedAttachments, ixConn.getCONST().LOCK.NO);
              
                logger.info(`Start uploading attachments`);
                for (var j = 0; j < details.anhaenge.length; j++) {
                    try {
                        var config = await getDocumentConfig(details.anhaenge[j]);
                        logger.info(`Create attachment with type ${config.type} and name ${config.name}`);
                        await createSord(config, candidate.data.objId);
                        logger.info("Attachment created");
                    } catch (e) {
                        logger.error(`Error creating attachment: ${e}`);
                        continue;
                    }
                }

                try {
                    logger.info(`Check if the new candidate exists`);
                    candidateExists = await checkCandidateExists(details.id);
                } catch (e) {
                    continue;
                }
            }

            //ensure everything is ok, than delete the candidate
            try {
                logger.info("Check if all attachments are uploaded");
                var allAttachmentsDone = await checkHasAllAttachments(details.anhaenge.length, candidateExists.objid);

                //time to clean up
                if (allAttachmentsDone) {
                    var hasAllAttachments = [new de.elo.ix.client.KeyValue("HAS_ALL_ATTACHMENTS", 1)];
                    ixConn.ix().checkinMap(ixConn.getCONST().MAP_DOMAIN.DOMAIN_SORD, candidateExists.objid, candidateExists.objid, hasAllAttachments, ixConn.getCONST().LOCK.NO);

                    try {
                        logger.info(`Create workflow for recruiter, if not exists`);
                        await startWorkFlow(exists, candidateExists.objid, details);
                    } catch (e) {
                        logger.error(`Error starting workflow: ${e}`);
                        continue;
                    }

                    logger.info("Delete application, all attachments are uploaded");
                    axiosRetry(axios, { retries: 3 });
                    axios.delete(`https://fuchs-soehne.info/resources/karriere/api/bewerbungen/${details.id}`, {
                        auth: { username: "elo_api", password: "EloApi123" }
                    }).then(response => {
                        logger.info("Application deleted!");
                    }).catch(err => { logger.error(`Error delete application: ${err}`); });
                }
                // try attachments again
                else {
                    var expectedAttachments = [new de.elo.ix.client.KeyValue("EXPECTED_ATTACHMENTS", details.anhaenge.length)];
                    ixConn.ix().checkinMap(ixConn.getCONST().MAP_DOMAIN.DOMAIN_SORD,  candidateExists.objid,  candidateExists.objid, expectedAttachments, ixConn.getCONST().LOCK.NO);
                  
                    ixConn.ix().checkoutSord(candidateExists.objid, ixConn.getCONST().SORD.mbLean, ixConn.getCONST().LOCK.NO, function res(s) {
                        var now = moment();
                        var iDate = moment(s.IDateIso, "YYYYMMDDHHmmss");
                        var diff = now.diff(iDate, 'hours');
                        if (diff >= 1) {
                            //Start WF even if not all attachments are uploaded - candidate is an hour old
                            try {
                                logger.info(`Create workflow for recruiter - candidate is an hour old, maybe the attachments are not complete`);
                                return startWorkFlow(exists, candidateExists.objid, details);
                            } catch (e) {
                                logger.error(`Error starting workflow: ${e}`);
                            }
                        }
                    });


                    logger.info(`Found: ${details.anhaenge.length} attachments, try reupload`);
                    for (var j = 0; j < details.anhaenge.length; j++) {
                        try {
                            var config = await getDocumentConfig(details.anhaenge[j]);
                            try {
                                var exists = ixConn.ix().checkoutSord("ARCPATH[" + candidateExists.objid + "]:\u00b6" + config.name, SordC.mbAll, LockC.NO);
                                logger.info(`Attachment not found!`);
                            } catch (e) {
                                //attachment is missing
                                logger.info(`Attachment not found! Create attachment with type ${config.type} and name: ${config.name}`);
                                await createSord(config, candidateExists.objid);
                            }
                        } catch (e) {
                            logger.error(`Error in reupload attachments: ${e}`);
                            continue;
                        }
                    }
                }

            } catch (e) {
                logger.error(`ensure everything is ok got an error: ${e}`);
                continue;
            }

        } catch (e) {
            logger.error("Error getting details for Application: " + allApplications[i].id + " | Error: " + e);
            continue;
        }
    }
};

async function startWorkFlow(exists, candidateObjid, details) {
    return new Promise((resolve, reject) => {
        try {
            ixConn.ix().checkoutSord(exists.data[0].objid, ixConn.getCONST().SORD.mbAll, ixConn.getCONST().LOCK.NO, function res(s) {
                var wfstarted = false;

                var findInfo = new de.elo.ix.client.FindWorkflowInfo();
                findInfo.inclDeleted = false;
                findInfo.objId = candidateObjid;
                findInfo.templateId = "sol.recruiting.Candidate.Create";

                ixConn.ix().findFirstWorkflows(findInfo, 1, ixConn.getCONST().WORKFLOW.mbAll, function (findResult) {
                    if (findResult.workflows.length < 1) {

                        for (var i = 0; i < s.objKeys.length; i++) {
                            if (s.objKeys[i].name == "RECRUITING_REQUISITION_RECRUITER") {
                                if (s.objKeys[i].data[0] != "") {
                                    wfstarted = true;
                                    ixConn.ix().checkoutUser(s.objKeys[i].data[0], ixConn.getCONST().CHECKOUT_USERS.BY_IDS, ixConn.getCONST().LOCK.NO, function result(userInfo) {
                                        var flowInfo = new de.elo.ix.client.StartWorkflowInfo();
                                        flowInfo.flowName = "Bewerbung " + details.vorname + " " + details.name;
                                        flowInfo.flowOwner = userInfo.id;
                                        flowInfo.templFlowId = "sol.recruiting.Candidate.Create";
                                        ixConn.ix().startWorkFlow2(candidateObjid, flowInfo, function r() {
                                            logger.info(`Workflow started for recuiter`);
                                            resolve(true);
                                        });
                                    });
                                }
                            }
                        }
                        if (wfstarted == false) {
                            var flowInfo = new de.elo.ix.client.StartWorkflowInfo();
                            flowInfo.flowName = "Bewerbung " + details.vorname + " " + details.name;
                            flowInfo.flowOwner = "36";
                            flowInfo.templFlowId = "sol.recruiting.Candidate.Create";
                            ixConn.ix().startWorkFlow2(candidateObjid, flowInfo, function r() {
                                logger.info(`Workflow started for group`);
                                resolve(true);
                            });
                        }
                    }
                });
            });
        } catch (e) {
            reject(e);
        }
    });

}

function getBearer() {
    return new Promise((resolve, reject) => {
        axiosRetry(axios, { retries: 3 });
        axios.post('https://fuchs-soehne.info/resources/security/accesstoken', { userid: "elo_api", password: "EloApi123" }).then(response => {
            resolve(response.data.token);
        }).catch(err => {
            reject(err);
        });
    });
};

function getDocumentConfig(a) {
    return new Promise((resolve, reject) => {
        getBearer().then((token) => {
            axiosRetry(axios, { retries: 3 });
            axios.get(a, { responseEncoding: 'binary', responseType: 'arraybuffer', headers: { Authorization: `Bearer ${token}` } }).then(response => {
                var typeName = response.headers['content-disposition'].match(/filename="(.{1,}\.\w{1,})"/)[1].split(".");
                var conf = {
                    type: typeName.pop(),
                    name: typeName.join("."),
                    base64Content: response.data.toString('base64')
                };

                resolve(conf);
            }).catch(err => { logger.err(err); reject(err) });
        }).catch(err => {
            reject(err);
        });
    });
}

function createSord(data, objId) {
    return new Promise((resolve, reject) => {
        if (data.base64Content.length == 0) {
            reject("createSord for attachment - base64Content is empty! - cannot be uploaded to elo... please check the online portal");
        }
        var config = {
            base64Content: data.base64Content,
            cfg: {
                maskName: "Dokument",
                pictureName: data.name,
                extension: data.type,
                type: data.type,
            },
            extension: data.type,
            objId: objId
        };
        try {
            ixConn.ix().executeRegisteredFunctionString("RF_sol_common_document_service_UploadFile", JSON.stringify(config), function (res) {
                if (res) {
                    logger.info("upload was successful! - res:" + res)
                    resolve();
                } else {
                    reject("no result from upload file - check ix log!");
                }
            });
        } catch (e) {
            reject(e);
        }
    });
};

function checkCandidateExists(id) {
    return new Promise((resolve, reject) => {
        try {
            var cfg = {
                asAdmin: true,
                log: "RF_ResultSet",
                connNr: 1,
                query: `select mapid as objid from eloFUCHS.dbo.map_objekte m inner join eloFUCHS.dbo.objekte o on m.mapid = o.objid where mapkey = 'RECRUITING_CANDIDATE_INTERFACE_ID' and mapvalue = '${id}' and o.objstatus = 0`
            };
            var res = JSON.parse(ixConn.ix().executeRegisteredFunctionString("RF_ResultSet", JSON.stringify(cfg)));
            if (res && res.data.length >= 1) {
                resolve(res.data[0]);
            } else {
                resolve(false);
            }
        } catch (e) {
            logger.error("checkCandidateExists - error:" + e);
            reject(e);
        }
    });
}

function createCandidate(exists, applicationDetails) {
    return new Promise(async (resolve, reject) => {
        try {
            var items = ixConn.ix().checkoutMap("objekte", exists.data[0].objid, null, de.elo.ix.client.LockC.NO).mapItems;
            console.log(items.RECRUITING_REQUISITION_GUID.value);
            var anrede = "";
            var gender = "";
            var geburtsdatum = "";
            var id = applicationDetails.id
            switch (applicationDetails.anrede) {
                case "Männlich":
                    gender = "M - Männlich";
                    anrede = "Sehr geehrter Herr";
                    break;
                case "Weiblich":
                    gender = "F - Weiblich";
                    anrede = "Sehr geehrte Frau";
                    break;
                case "Divers":
                    gender = "X - Divers";
                    anrede = "Guten Tag";
                    break;
            }
            if (applicationDetails.geburtsdatum) {
                geburtsdatum = moment(applicationDetails.geburtsdatum).format("YYYYMMDD");
            }
            //Bewerberakte anlegen
            var cfg = {
                "template": {
                    "name": "Default"
                },
                "sordMetadata": {
                    "mapKeys": {
                        "RECRUITING_REQUISITION_GUID": items.RECRUITING_REQUISITION_GUID.value,
                        "RECRUITING_POSTING_GUID": applicationDetails.jobId,
                        "RECRUITING_CANDIDATE_STATE": "",
                        "RECRUITING_CANDIDATE_PRIVATEEMAIL": applicationDetails.email,
                        "RECRUITING_CANDIDATE_PRIVATEMOBILE": applicationDetails.tel,
                        "RECRUITING_CANDIDATE_STREETADDRESS": applicationDetails.anschrift.strasse,
                        "RECRUITING_CANDIDATE_PRIVATEPHONE": applicationDetails.tel,
                        "RECRUITING_CANDIDATE_INTERFACE_ID": id,
                        "RECRUITING_ATTENTION": applicationDetails.aufmerksam,
                        "CANDIDATE_ANREDE": anrede
                    },
                    "objKeys": {
                        "RECRUITING_CANDIDATE_FIRSTNAME": applicationDetails.vorname,
                        "RECRUITING_CANDIDATE_LASTNAME": applicationDetails.name,
                        "RECRUITING_CANDIDATE_CITY": applicationDetails.anschrift.ort,
                        "RECRUITING_CANDIDATE_COUNTRY": applicationDetails.anschrift.land,
                        "SOL_TYPE": "RECRUITING_CANDIDATE",
                        "RECRUITING_CANDIDATE_POSTALCODE": applicationDetails.anschrift.plz,
                        "RECRUITING_CANDIDATE_PRIVATEEMAIL": applicationDetails.email,
                        "RECRUITING_CANDIDATE_BIRTHDAY": geburtsdatum,
                        "RECRUITING_CANDIDATE_GENDER": gender
                    }
                }
            };
            var res = await ixConn.ix().executeRegisteredFunctionString("RF_sol_recruiting_function_CreateCandidateHeadlessStrict", JSON.stringify(cfg));
            res = JSON.parse(res);
            res.candidateName = applicationDetails.vorname + " " + applicationDetails.name;
            res.candidateId = id;
            resolve(res);

        } catch (e) {
            logger.error("createCandidate - error:" + e);
            reject(false);
        }
    });
}

function getApplicationDetails(id) {
    return new Promise((resolve, reject) => {
        axiosRetry(axios, { retries: 3 });
        axios.get(`https://fuchs-soehne.info/resources/karriere/api/bewerbungen/${id}`, { auth: { username: "elo_api", password: "EloApi123" } }).then(response => {
            resolve(response.data);
        }).catch(err => { resolve(null) });
    });
}

function checkRequisitionExists(jobId) {
    return new Promise((resolve, reject) => {
        try {
            var cfg = {
                asAdmin: true,
                log: "RF_ResultSet",
                connNr: 1,
                query: `select mapid as objid from eloFUCHS.dbo.map_objekte m inner join eloFUCHS.dbo.objekte o on m.mapid = o.objid where mapkey ='POSTING_ID' and mapvalue = '${jobId}' and o.objstatus = 0`
            };
            var res = ixConn.ix().executeRegisteredFunctionString("RF_ResultSet", JSON.stringify(cfg));
            res = JSON.parse(res);

            resolve(res);
        }
        catch (e) {
            reject(e);
        }
    });
}

function checkHasAllAttachments(attachmentCount, objid) {
    return new Promise((resolve, reject) => {
        try {
            var cfg = {
                asAdmin: true,
                log: "RF_ResultSet",
                connNr: 1,
                query: `select COUNT(*) as 'count' from eloFUCHS.dbo.objekte where objparent ='${objid}' and objstatus = 0`
            };
            var res = JSON.parse(ixConn.ix().executeRegisteredFunctionString("RF_ResultSet", JSON.stringify(cfg)));

            if (res && res.data) {
                logger.info(`Found ${JSON.stringify(res.data[0].count)} / ${attachmentCount} attachments`);
                if (String(attachmentCount) == String(res.data[0].count)) {
                    resolve(true);
                }
            }
            resolve(false);
        }
        catch (e) {
            logger.error(e);
            reject(e);
        }
    });
}

process.on('uncaughtException', function (er) {
    logger.error(er);
    process.exit(1)
})

AdjustApplications();
setInterval(AdjustApplications, 600000);

app.listen(3000);
console.log("App listen on Port 3000");
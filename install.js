const Service = require('node-windows').Service;

// Create a new service object
const svc = new Service({
    name: 'SiRecruitingInterface',
    description: 'SiRecruitingInterface (SideStep Business Solutions GmbH)',
    script: __dirname + '\\RecruitingInterface.js',
    env:[{
        name: "NODE_ENV",
        value: "production"
    }]
    //, workingDirectory: '...'
    //, allowServiceLogon: true
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
    svc.start();
});

svc.install();

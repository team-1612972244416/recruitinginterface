const http = require('http');
const https = require('https');

module.exports = function (config) {
    return {
        config: config,
        request: (config) => {
            return new Promise((resolve, reject) => {
                try {
                    console.log("start promise");
                    let module = (config.protocol === "https" ? https : http);

                    const req = module.request(config.request, (res) => {

                        const { statusCode } = res;
                        const contentType = res.headers['content-type'];

                        let error;
						
						console.log(statusCode);
                        if (statusCode !== 200) {
                            error = new Error('Request Failed.\n' +
                                `Status Code: ${statusCode}`);
                        } else if (!/^application\/json/.test(contentType)) {
                            error = new Error('Invalid content-type.\n' +
                                `Expected application/json but received ${contentType}`);
                        }
                        if (error) {
                            console.log("start error");
                            res.resume();
                            try {
                                console.log("try reject");
                                reject(error.message);
                            } catch (e) {

                                console.log(e);
                                console.log("catch reject");
                                reject(e.message);
                            }

                            //return;
                            reject();
                            res.statusCode = 500;
                            

                        } else {


                            res.setEncoding(config.encoding);
                            let rawData = '';
                            res.on('data', (chunk) => {
                                rawData += chunk;
                            });
                            res.on('end', () => {
                                try {
                                    resolve(JSON.parse(rawData));
                                } catch (e) {
                                    throw e;
                                }
                            });
                        }
                    }).on('error', (e) => {
                        throw e;
                    });
                    if (config.request.headers) {
                        req.headers = config.request.headers;
                    }
                    if (config.request.body) {
                        req.write(JSON.stringify(config.request.body));
                    }
                    req.end();
                } catch (e) {
                    reject(e);
                }
            });
        },
        on: function () {

        },
        post: function (config) {

        }
    }
}
